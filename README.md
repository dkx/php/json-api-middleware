# DKX/PHP/JsonApiMiddleware

Middleware for json api.

**Based on [dkx/json-api](https://gitlab.com/dkx/php/json-api).**

## Installation

```bash
$ composer require dkx/json-api-middleware
``` 

## Usage

```php
<?php

use DKX\JsonApi\Manager;
use DKX\JsonApiMiddleware\JsonApiMiddleware;

$manager = new Manager;
$middleware = new JsonApiMiddleware($manager, 'include');
```

**Example controller (Slim):**

```php
<?php

use DKX\JsonApiMiddleware\ItemResponse;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;

class DetailBookController
{
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
    {
        $book = loadBookById($args['id']);
        return new ItemResponse($response, $book);
    }
}
```

Or simply use `DKX\JsonApiMiddleware\CollectionResponse` when you need to transform an array of items.

## Pagination

It's easy to use the `CollectionResponse` for small amounts of data, but for larger sets, you should use pagination.

This library works with [dkx/paginator](https://gitlab.com/dkx/php/paginator).

```php
<?php

use DKX\JsonApiMiddleware\PaginatedResponse;
use DKX\Paginator\PaginatedData;

$currentPage = 1;
$itemsPerPage = 20;
$paginator = $paginatorFactory->create($totalCount, $currentPage);

$totalCount = getAllBooksCount();
$books = loadAllBooksWithPaginator($paginator);

$data = new PaginatedData($paginator, $books);
$response = new PaginatedResponse($response, $data);
```
