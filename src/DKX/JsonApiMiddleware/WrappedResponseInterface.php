<?php

declare(strict_types=1);

namespace DKX\JsonApiMiddleware;

use DKX\JsonApi\Manager;
use Psr\Http\Message\ResponseInterface;

interface WrappedResponseInterface
{


	/**
	 * @param \DKX\JsonApi\Manager $manager
	 * @param string[] $include
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function unwrapResponse(Manager $manager, array $include): ResponseInterface;

}
