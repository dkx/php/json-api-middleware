<?php

declare(strict_types=1);

namespace DKX\JsonApiMiddleware;

use DKX\JsonApi\Manager;
use DKX\WrappedHttpResponse\WrappedHttpResponse;
use Nette\Utils\Json;
use Psr\Http\Message\ResponseInterface;

abstract class WrappedResponse extends WrappedHttpResponse implements WrappedResponseInterface
{


	/**
	 * @param \DKX\JsonApi\Manager $manager
	 * @param string[] $include
	 * @return mixed[]
	 */
	protected abstract function toJsonApiData(Manager $manager, array $include): array;


	/**
	 * @param \DKX\JsonApi\Manager $manager
	 * @param string[] $include
	 * @return \Psr\Http\Message\ResponseInterface
	 * @throws \Nette\Utils\JsonException
	 */
	public function unwrapResponse(Manager $manager, array $include): ResponseInterface
	{
		$response = $this->innerResponse;
		$data = $this->toJsonApiData($manager, $include);
		$body = $response->getBody();
		$body->write(Json::encode($data));

		return $response->withHeader('Content-Type', 'application/json;charset=utf-8');
	}

}
