<?php

declare(strict_types=1);

namespace DKX\JsonApiMiddleware;

use DKX\JsonApi\Manager;
use Psr\Http\Message\ResponseInterface;

final class CollectionResponse extends WrappedResponse
{


	/** @var mixed[] */
	private $data;

	/** @var mixed[] */
	private $meta;

	/** @var mixed[] */
	private $contextAttributes;


	/**
	 * @param \Psr\Http\Message\ResponseInterface $innerResponse
	 * @param mixed[] $data
	 * @param mixed[] $meta
	 * @param mixed[] $contextAttributes
	 */
	public function __construct(ResponseInterface $innerResponse, array $data, array $meta = [], array $contextAttributes = [])
	{
		parent::__construct($innerResponse);

		$this->data = $data;
		$this->meta = $meta;
		$this->contextAttributes = $contextAttributes;
	}


	/**
	 * @param \DKX\JsonApi\Manager $manager
	 * @param string[] $include
	 * @return mixed[]
	 */
	protected function toJsonApiData(Manager $manager, array $include): array
	{
		return $manager->collectionToArray($this->data, $include, $this->meta, $this->contextAttributes);
	}

}
