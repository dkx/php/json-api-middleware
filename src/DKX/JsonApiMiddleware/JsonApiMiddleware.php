<?php

declare(strict_types=1);

namespace DKX\JsonApiMiddleware;

use DKX\JsonApi\Manager;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

final class JsonApiMiddleware implements MiddlewareInterface
{


	/** @var \DKX\JsonApi\Manager */
	private $manager;

	/** @var string|null */
	private $include;


	public function __construct(Manager $manager, ?string $include = null)
	{
		$this->manager = $manager;
		$this->include = $include;
	}


	public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
	{
		$response = $handler->handle($request);

		if ($response instanceof WrappedResponseInterface) {
			$include = [];

			if ($this->include !== null) {
				$params = $request->getQueryParams();
				if (\array_key_exists($this->include, $params)) {
					$include = \explode(',', $params[$this->include]);
				}
			}

			return $response->unwrapResponse($this->manager, $include);
		}

		return $response;
	}

}
