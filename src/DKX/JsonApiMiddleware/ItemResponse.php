<?php

declare(strict_types=1);

namespace DKX\JsonApiMiddleware;

use DKX\JsonApi\Manager;
use Psr\Http\Message\ResponseInterface;

final class ItemResponse extends WrappedResponse
{


	/** @var object */
	private $item;

	/** @var mixed[] */
	private $meta;

	/** @var mixed[] */
	private $contextAttributes;


	/**
	 * @param \Psr\Http\Message\ResponseInterface $innerResponse
	 * @param object $item
	 * @param mixed[] $meta
	 * @param mixed[] $contextAttributes
	 */
	public function __construct(ResponseInterface $innerResponse, object $item, array $meta = [], array $contextAttributes = [])
	{
		parent::__construct($innerResponse);

		$this->item = $item;
		$this->meta = $meta;
		$this->contextAttributes = $contextAttributes;
	}


	/**
	 * @param \DKX\JsonApi\Manager $manager
	 * @param string[] $include
	 * @return mixed[]
	 */
	protected function toJsonApiData(Manager $manager, array $include): array
	{
		return $manager->itemToArray($this->item, $include, $this->meta, $this->contextAttributes);
	}

}
