<?php

declare(strict_types=1);

namespace DKX\JsonApiMiddleware;

use DKX\JsonApi\Manager;
use DKX\Paginator\PaginatedDataInterface;
use DKX\WrappedHttpResponse\WrappedHttpResponse;
use Psr\Http\Message\ResponseInterface;

final class PaginatedResponse extends WrappedHttpResponse implements WrappedResponseInterface
{
	/**
	 * @param ResponseInterface $innerResponse
	 * @param PaginatedDataInterface $data
	 * @param mixed[] $contextAttributes
	 */
	public function __construct(ResponseInterface $innerResponse, PaginatedDataInterface $data, array $contextAttributes = [])
	{
		$paginator = $data->getPaginator();

		parent::__construct(new CollectionResponse($innerResponse, $data->getData(), [
			'pagination' => [
				'page' => $paginator->getPage(),
				'itemsPerPage' => $paginator->getItemsPerPage(),
				'totalCount' => $paginator->getTotalCount(),
			],
		], $contextAttributes));
	}

	/**
	 * @param \DKX\JsonApi\Manager $manager
	 * @param string[] $include
	 * @return \Psr\Http\Message\ResponseInterface
	 * @throws \Nette\Utils\JsonException
	 */
	public function unwrapResponse(Manager $manager, array $include): ResponseInterface
	{
		/** @var \DKX\JsonApiMiddleware\CollectionResponse $response */
		$response = $this->innerResponse;
		return $response->unwrapResponse($manager, $include);
	}
}
